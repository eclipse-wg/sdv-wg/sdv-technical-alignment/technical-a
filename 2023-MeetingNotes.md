# Tech Alignment Call - Meeting Notes 2023

# Bi-weekly on Tuesdays
# Zoom link 
https://eclipse.zoom.us/j/85285451901?pwd=NzZFWU5KNkFPc3BwUVovbWc0akt4QT09

# Meeting Agenda 12/12/2023
- Intro/show&tell w3c VISS & [Eclipse Kuksa.val](https://github.com/eclipse/kuksa.val) - Erik Jaegervall (Bosch)
- [ThreadX](https://threadx.io) intro - Frederic Desbiens

# Meeting Agenda 14/11/2023
- Intro/show&tell [Eclipse Kanto](https://eclipse.dev/kanto/) - Todorov Teodor (Bosch)
- tbd: update on uProtocol SDK status and roadmap - Steven Hartley (GM)


# Meeting Agenda 19/09/2023
- Introduction to [openDuT](https://projects.eclipse.org/proposals/eclipse-opendut) - Thomas Irmscher  (ETAS), Johannes Baumgartl (Mercedes-Benz Tech Innovation)
- [Slides](20230919-Eclipse-openDuT.pdf)
 

# Meeting Agenda 05/09/2023
- Introduction to Eclipse BlueChi (Pierre-Yves- RedHat)
- [Slides](https://pingou.fedorapeople.org/slides/2023_Eclipse_SDV_BlueChi.pdf)
- Project repository:
  [https://github.com/containers/bluechi](https://github.com/containers/bluechi)
  (will move once transition to Eclipse SDV has completed)

# Meeting Agenda 25/07/2023
- Software Package Data eXchange (SPDX) Introduction - Kate Steward (Linux Foundation)

# Meeting Agenda 27/06/2023
- (teaser) Synergies between SDV and OpenMDM - Hand-Dirk Walter (Karakun)
- Overview of Cummins Cosmos and feedback on potential integration into other Eclipse Projects - Ankit Tarkas (Cummins)
- SDV at ETAS Connections in Stuttgart: Introducing SDV activities to other organizations. Call for participation. 

# Meeting Agenda and Minutes  30/05/2023
- Rust-based Container Runtime - Quark - Jeffers Liu & Yulin Sun (FutureWei)
Points touched: What is Quark? Comparisson on Quark usage vs. other possibilities. Why Quark is the better choice?

# Meeting Agenda and Minutes  16/05/2023
- MoEc Project presentation - Dirk Bangel (Bosch)
- Communication protocols - kick off new breakout group 
  - [project page](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/communication-protocols-sdv-topics)
  - [wiki](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/communication-protocols-sdv-topics/-/wikis/home)
  - Facilitator: Steven Hartley 

- [Community days](https://docs.google.com/presentation/d/1iec4LPxjCYF546Q7Xfg0aTDxlm9lNHEK8Q1kBr8OOeM/edit?usp=sharing) - Call for technical talks and projects proposals  

Notes:
- MoEc presentation - link 
  - predevelopment SDK kit
  - there is production ready code, the release in underdiscussion internally
  - map ROS context projects to SDV - Gabriele and Dirk take the point 

- Communication protocols
  - uprotocol code should be available by this week 
  - communication group started - Steven to follow up with communications 

# Meeting Agenda and Minutes  02/05/2023
Follow up panel
- Communication protocols - panel discussion
  - uProtocol - Steven Hartley (GM), Eclipse Zenoh Gabriele Baldoni and Luca Cominardi (Zettascale)

Notes:
- started with quick recap from various people about what uProtocol is/does
- discussed interop/integration option between uProtocol, zenoh, Chariott 
  - zenoh would at the very least be an interesting transport option for uProtocol
  - Chariott is looking into integrating uProtocol as a service fabric option
- discussion then went into the topic area of permission/authorization approaches for an SDV-style sw programming model
  - complete implied scope for this is probably huge (from secure execution to extensive key mgmt capabilites to app-level permission semantics)
  - this topic area - for system landscaped like in automotive - doesn't seem to be addressed much by open development efforts
  - might be an area of engagement for an Eclipse SDV project(s), but only makes sense if there is a path to productization from the beginning...
  - ... plus scoping of the problem to solve would be essential

# Meeting Agenda and Minutes  18/04/2023
- Breakout group SW Orchestration: 
  - Holger Dormann (Elektrobit) to present [Eclipse Ankaios](https://projects.eclipse.org/proposals/eclipse-ankaios)
    - find the presentation [here](./20230418_Eclipse_SDV_Ankaios.pdf)
  - Panel discussion

- Communication protocols - panel discussion
  - uProtocol - Steven Hartley (GM), Eclipse Zenoh Gabriele Baldoni and Luca Cominardi (Zettascale)

Notes: 

Ankaios
- Zenoh might be a useful communication fabric
- QM workloads vs safety? Currently only QM
- no Authentication yet
- POSIX system needed (pipes)
- what about self-update? could use Ankaios to manage a self-update application...
- deep-dive session to continue lively QA discussion

Communication item
- elevator pitches for both zenoh and uProtocol
- deep-dive session (maybe start of commmunication break-out group)

# Meeting Agenda and Minutes  04/04/2023
- Community Days open mic - Daniel
  - Overall great collegiality and technical exchange 
  - Daniel's highlight: uProtocol: first SDV production ready SW stack
  - Daniel's take on tech breakout groups: 
    - Testing & Validation: group will follow up with hands on implementation - Johannes
    - SDV use cases: proposal to call these references "blueprints" targeted to be published in the Leda incubator. Go forward with monthly meeting. - Naci 
    - SW orchestration: group is aligned fairly well, some projects are growing in this area - Kai 
- [Eclipse Zenoh introduction](./2023.04.04-zenoh.pdf) - Gabriele Baldoni and Luca Cominardi (Zettascale)
  - looping in Zeno with the use cases group 

# Meeting Agenda and Minutes 21/03/2023 
- Teaser: Intro to AutoSD - [Leonardo Rossetti](lrossett@redhat.com) (RedHat)


# Meeting Agenda and Minutes 07/03/2023 
- Teaser: ![Eclipse Theia teaser](./EclipseTheia-Teaser)
- SOAFEE orchestration [website](https://hackweek.opensuse.org/projects/real-time-container-runtime-support) 

# Meeting Agenda and Minutes 21/02/2023 
- Paul Boyes & Thomas Spreckley: COVESA introduction
  - Thomas: Velocitas and Kuksa Val are already using VSS 
  - Paul: Intro to Vehicle Signal Specifations - link to [ppt](./20230222-COVESA Intro to VSS Eclipse SDV.pdf)
    - how is VSS evolving: best practises are being compiled and improvements are user-input based
    - COVESA [FAQ file](./SDV-COVESA-FAQ.md)

# Meeting Agenda 07/02/2023 
- Daniel K: first thoughts on "what might 'automotive-grade' mean for us?"
- Jonas Wolf: show&tell gsn2x Project (https://github.com/jonasthewolf/gsn2x)
- merging of breakout groups - reminder: Orchestration and Scheduling 

# Meeting Agenda 24/01/2023 
- Thomas S [presentation](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/technical-a/-/blob/main/CES%20SdV%20Projects%20Overview.pptx) held at CES
- CES summary and impressions 
- Breakout group reminder: kickoff meetings starting next week
- merging the topics: scheduling-sdv-topic and sw orchestration 
- Use case/scenario collection for Community Days event
- Community Days [website](https://sdv.eclipse.org/sdv-community-day-lisbon-2023/) and [registration](https://www.eventbrite.com/e/sdv-community-day-lisbon-march-2023-tickets-520519135747) online, [agenda](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-community-days/2023-q1-community-days/-/wikis/home)
- open discussion 

## Use cases so far 
- truck fleet management: where trucks run SDV stacks so that logistics fleet operators can manage a diverse set of vehicles by having their fleet operator service supplier install data collection clients and other related features into the trucks, as 3rd party SDV-style applications.
- dynamic car insurance: where insurance companies offer rates that dynamically adapt to driving behavior, implemented via 3rd party SDV-style data collection/evaluation applications that run on SDV stacks in customer vehicles. 

## Meeting notes 
- Decision: OK to merge the two breakout groups: scheduling and SW orchestration


# Meeting Agenda 09/01/2023 
- Recap on initiatives & landscape vision 
- Thomas S presentation held at CES - *moved to next meeting*
- CES summary and impressions - *moved to next meeting*

## Meeting notes 
### Other SDV related initiatives - What is the landscape around EF SDV

Recap of other initiatives open source or not, that are related to EF SDV.
SOAFEE and Eclipse SDV are orthogonal initiatives, but some EF projects such as Leda and Chariott are bridging the gap. 
Kuksa and Velocitas are implementing COVESA VSS. Velocitas is also using the digital.auto platform. 

![MapAutoSWInitiatives](./MapAutoSWInitiatives.png)

![SOAFEEandEFSDV](./SOAFEEandEFSDV.png)

![CovesaAndAutosar.png](./CovesaAndAutosar.png)

## Review of the break out group topics

Deadline for signing up it this week. An [overview](https://gitlab.eclipse.org/groups/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/-/wikis/home) of the topics is to be found here. 

## Review of the SDV landscape

*Call to action*: project participants please help us sort your projects in the correct box! 
Topic area to explore is the definition of requirements, a starting point could the insurance companies requirements (https://openinsurance.io/). This topic could be explored in the ["Testing and Validation"](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/testing-validation-sdv-topic/-/wikis/home) group. 


![Landscape.png](./Landscape.png)


