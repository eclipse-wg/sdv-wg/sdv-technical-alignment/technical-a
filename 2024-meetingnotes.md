# Tech Alignment Call - Meeting Notes 2024

# Monthly on Tuesdays
# Zoom link 
Join Zoom Meeting
https://eclipse.zoom.us/j/82003582354?pwd=6ujv9HyYT28ab6wHpaNStmqoXjM1Mh.1

Meeting ID: 820 0358 2354
Passcode: 441325

# Meeting Agenda 03/12/2024
- Introduction to SODA Sim (https://soda.auto/tools/sim/index.html), with Christoforos Chatzikomis. Meeting to be recorded. 

# Meeting Agenda 19/11/2024
- Managing Safety Requirements with BASIL (https://github.com/elisa-tech/BASIL), with Luigi Pellecchia. Meeting to be recorded. 

# Meeting Agenda 05/11/2024
- Sphinx Needs from the source, with Daniel Woste. Meeting to be recorded. 

# SUMMER BREAK until SEPTEMBER 

# Meeting Agenda 30/07/2024
- Android Automotive, AOSP - community pointers and related activities. Meeting to be recorded. 
  - session recording: https://drive.google.com/file/d/17bUereTdKZfcyIXgIVmQXPN1tMVTdVhJ/view?usp=sharing

# Meeting Agenda 16/07/2024
- OxidOS and it's journey certifying for ISO26262 ASIL-D (OxidOS). Meeting to be recorded. 

# Meeting Agenda 25/06/2024
- Safety compliance of Rust libraries by Amit Dharmapurikar (Thoughtworks). Meeting to be recorded. 

# Meeting Agenda 11/06/2024
- Eclipse Kanto.auto presentation. Meeting to be recorded. 

# Meeting Agenda 28/05/2024
- MOVED TO 11/06 - Eclipse Kanto.auto presentation.


# Meeting Agenda 27/02/2024
- Eclipse Symphony
