# Tech Alignment Call - Meeting Notes 2025

# Monthly on Tuesdays
# Zoom link 
Join Zoom Meeting
https://eclipse.zoom.us/j/82003582354?pwd=6ujv9HyYT28ab6wHpaNStmqoXjM1Mh.1

Meeting ID: 820 0358 2354
Passcode: 441325

# Meeting Agenda 11/03/2025
- [Rust tech special] Rust in AUTOSAR, by Christof Petig. Meeting to be recorded. 

# Meeting Agenda 25/02/2025
- [Rust tech special] Self-referential structs in Rust, by Peter LeVasseur. Meeting to be recorded. 

# Meeting Agenda 28/01/2025
- Intro to new WG member TiHAN-IIT Hyderabad, by Sindhumitha Kulandaivel. Meeting to be recorded. 
https://eclipse.zoom.us/rec/share/Ua-EyNFTT5cd0SuA7qcxoMI-1IHpZXeqNKrCl3G-2ZIhWqI65BD6Go938Z-1Avf4.-SpQ5ZUuy36LxKz-?startTime=1738076203000
Passcode: v.vUH^b0

# Meeting Agenda 14/01/2025
- Introduction Eclipse Safe Open Vehicle Core (https://projects.eclipse.org/proposals/eclipse-safe-open-vehicle-core), with Thilo Schmitt and Holger Grandy. Meeting to be recorded. 

Recordings here https://eclipse.zoom.us/rec/share/Hu2U4J8o2z9dYygP1SRjiybuTydmsrahiiS6WdinkQFXHBaoRovZ1094jSZsePwr.n8PrYAOq8D3Tjliv?startTime=1736866563000
Passcode: k#3FNJP^
